# Portfolio Aarón Puche Benedito

## ES
Hola, me llamo Aarón Puche, soy Ingeniero informático con más de 4 años de experiencia en el sector. Estudie en la Universitat Politècnica de Valencia (UPV) y he trabajado mayoritariamente en la rama del desarrollo web. En la última empresa donde estuve colaboré en proyectos web con BBVA o Cámara de Valencia. A principios de 2021 decidí dar un giro a mi vida profesional y estudiar el Máster de Ciencia de Datos en la Universitat Oberta de Catalunya (UOC), estudios que he finalizado recientemente.

En este repositorio encontraremos proyectos relacionados con la ciencia de datos. Dentro de la carpeta *Proyectos académicos* encontraremos tres bloques con los proyectos que he realizado durante el estudio del máster: análisis de grafos y redes sociales, machine learning y procesamiento del lenguaje natural.

En el primer bloque podemos encontrar proyectos relacionados con el análisis de redes y las interacciones de sus elementos, en el segundo encontraremos proyectos donde se han aplicado distintas técnicas de machine learning como minería de datos, clustering, regresión o clasificación. En este segundo bloque se hace uso de los principales algoritmos como k-means, linear regression, random forest, k-nearest neighbors, etc.
En el último bloque encontraremos proyectos relacionados con el análisis de textos como la detección de las principales temáticas de una serie de documentos, el análisis de sentimientos, la detección de noticias falsas o la traducción automática mediante deep learning.

Por otro lado, en la carpeta *Proyectos personales* encontraremos aquellos proyectos que me han despertado un interés o una curiosidad especial más allá del ámbito académico de la UOC.

## CAT
Hola, em diuen Aarón Puche Benedito, soc enginyer informàtic amb més de 4 anys d'experiència en el sector. Vaig estudiar en la Universitat Politècnica de València (UPV) i he treballat majoritàriament en la rama del desenvolupament web. En l'última empresa on estava vaig col·laborar en projectes amb BBVA o Cámara de València. A principis de 2021 vaig decidir fer un gir en la meua vida professional i estudiar el Màster de Ciència de Dades en la Universitat Oberta de Catalunya (UOC), estudis que he finalitzat recentment.

En aquest repositori trobarem projectes relacionats amb la ciència de dades. Dins la carpeta *Proyectos académicos* trobarem tres blocs amb els projectes que he realitzat durant l'estudi del màster: anàlisi de grafs i xarxes socials, machine learning i processament del llenguatge natural.

En el primer bloc podem trobar projectes relacionats amb l'anàlisi de xarxes i les interaccions amb els seus elements, en el segon trobarem projectes on s'han aplicat distintes tècniques de machine learning com mineria de dades, clustering, regressió o classificació. En aquest segon bloc es fa ús dels principals algoritmes com k-means, linear regression, random forest, k-nearest neighbors, etc. En el darrer bloc trobarem projectes relacionats amb l'anàlisi de textos com la detecció de les temàtiques principals d'una sèrie de documents, l'anàlisi de sentiments, la detecció de notícies falses o la traducció automàtica mitjançant deep learning.

Per altra banda, en la carpeta *Proyectos personales* trobarem aquells projectes que m'han despertat un interés o una curiositat especial més enllà de l'àmbit acadèmic de la UOC.

## EN
Hello, my name is Aarón Puche, I am a Computer Engineer with more than 4 years of experience. I studied at the Universitat Politècnica de Valencia (UPV) and I have worked in the field of web development. In the last company I collaborated on web projects with BBVA or Cámara de Valencia. At the beginning of 2021 I decided to take a turn in my professional life and study the Master in Data Science at the Universitat Oberta de Catalunya (UOC), studies that I have recently finished.

In this repository we will find projects related to data science. Inside the folder *Proyectos académicos* we will find three blocks with the projects that I have made out during the study of the master: graphs and social networks analysis, machine learning and natural language processing.

In the first block we can find projects related to the networks analysis  and the interactions of their elements, in the second, we will find projects where different techniques have been applied, such as data mining, clustering, regression or classification. In this second block, we use some of  the main algorithms of machine learning such as k-means, linear regression, random forest, k-nearest neighbors, etc. In the last block we will find projects related to text analysis such as topic detection, sentiment analysis, the detection of fake news or translation using deep learning.

In the other hand, in the folder *Proyectos personales*, we will find those projects that have aroused a special interest or curiosity in me beyond the academic sphere of the UOC.